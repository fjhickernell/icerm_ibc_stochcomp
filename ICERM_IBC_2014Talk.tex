%ICERM Talk, September 15, 2014
\documentclass[10pt,compress]{beamer} %slides and notes
\usepackage{amsmath,datetime,xmpmulti,mathtools,bbm,array,booktabs,alltt,xspace,mathabx,tikz,pifont,graphicx}
\usepackage[author-year]{amsrefs}
\usetikzlibrary{arrows}
\usetheme{FredIIT}

\setlength{\parskip}{2ex}
\setlength{\arraycolsep}{0.5ex}

\logo{\includegraphics[width=0.5cm]{MengerIITRedGray.pdf}}

%requires graphics files 
%   scrsob512pts.eps, walshk1fun.eps, walshk2fun.eps, walshk3fun.eps, walshk4fun.eps, walshk5fun.eps, walshk6fun.eps, PlotFWTCoefUse256.eps

\title{Adaptive Algorithms for Stochastic Computation}
\author[hickernell@iit.edu]{Fred J. Hickernell}
\institute{\small{Department of Applied Mathematics,  Illinois Institute of Technology \\
\href{mailto:hickernell@iit.edu}{\nolinkurl{hickernell@iit.edu}} \quad
\href{http://mypages.iit.edu/~hickernell}{\nolinkurl{mypages.iit.edu/~hickernell}} \\[2ex]
Joint work with Tony Jim\'enez Rugama\\ Tony, Yuhan Ding, and Xuan Zhou will present posters on Tuesday afternoon \\[2ex]
Supported by NSF-DMS-1115392 \\
Many thanks to the organizers}}
\date[ICERM IBC \& Stoch.\ Comp. 2014]{September 15, 2014}

%Monte Carlo and quasi-Monte Carlo methods are popular for computing i) means of random variables with complicated distributions, and ii) high dimensional integrals.  The number of samples required to obtain an approximation with a given accuracy depends on the variance of the random variable or the roughness of the integrand.  Adaptive methods determine how many samples are needed based on the observed random numbers or function values rather than on a priori information.  We show how to construct good (quasi-)Monte Carlo algorithms for cones of random variables or integrands.  In some cases we can also demonstrate the asymptotic optimality of these algorithms.  Since these cones are non-convex sets, adaption can have an advantage over non-adaptive algorithms.

\input FJHDef.tex

\DeclareMathOperator{\Var}{Var}
\DeclareMathOperator{\oerr}{\overline{err}}
\newcommand{\cube}{[0,1)^d}
\def\newblock{\hskip .11em plus .33em minus .07em}
\newcommand{\wcS}{\widecheck{S}}
\newcommand{\wcomega}{\mathring{\omega}}
\newcommand{\medcone}{\parbox{1.2cm}{\includegraphics[width=0.55cm,angle=270]{ProgramsImages/MediumWaffleCone.eps}}\xspace}

%\newcommand{\FJinitials}{F. J.} %To give my name to the bibliography
%\newcommand{\HickernellFJ}{Hickernell} %To give my name to the bibliography
\newcommand{\HickernellFJ}{H.} %To give my name to the bibliography
%\newcommand{\HickernellFJ}{FJH} %To replace my name with initials in citations

\begin{document}
\tikzstyle{every picture}+=[remember picture]

\frame{\titlepage}

\section{Problem}
\begin{frame}\frametitle{Some Problems in Stochastic Computation}
\vspace{-3ex}
\begin{tabular}{>{\flushleft}m{5.5cm}@{\qquad}>{\flushleft}m{5.5cm}}
\begin{gather*}
\mu=\Ex(Y) \only<1>{= ?}
\only<2->{\approx \hmu_n(\{Y_i\}) := \frac 1 n \sum_{i=1}^{n} Y_i}\\
\mu=\Ex[f(\vX)]=\int_{\reals^d} f(\vx) \, \varrho(\vx) \, \dif \vx 
\only<1>{= ?} \\
\mu=\Prob(a \le Y \le b) = ?\\
p=\Prob(Y \le \mu), \ \mu = ?
\end{gather*}
\uncover<3->{Given a \alert{tolerance} $\varepsilon$ how do we choose $n$ \alert{adaptively} to make 
\[
\abs{\mu - \hmu_n} \le \varepsilon
\]
(with high probability)?}
&
financial risk, statistical physics, photon transport, \ldots \\[1ex]
\uncover<2->{IID sampling, \alert{low discrepancy sampling}, error bounds, tractability, multi-level  \cites{Ric51,Nie92,SloJoe94,Hic97a,DicPil10a,NovWoz10a, DicEtal14a,Gil14a}, \ldots } \\[1ex]
\uncover<3->{guaranteed, adaptive Monte Carlo \cites{HicEtal14a, JiaHic16a}, trapezoidal rule \cite{HicEtal14b}, \alert{quasi-Monte Carlo} \cites{HicJim16a, JimHic16a}, GAIL \cite{ChoEtal14a}}
\tabularnewline
\end{tabular}
\end{frame}

\begin{frame}\frametitle{Recent Results}
\vspace{-2ex}
\begin{itemize}

\item $\mu=\Ex(Y)=?$  \cite{HicEtal14a}, for somewhat different view see \cite{BayEtal14a}
\begin{itemize}
\item Compute a highly probable upper bound on true variance, $\fC^2 \hsigma^2_{n_\sigma}$, using $n_\sigma$ IID samples.  

\item Use a Berry-Esseen inequality (finite sample Central Limit Theorem) to find $n$ such that $\Prob(\abs{\mu - \hmu_n} \le \varepsilon) \ge 99\%$. 

\item Guaranteed for random variables in the \alert{cone} \medcone of bounded kurtosis $\Ex[(Y-\mu)^4]/\sigma^4 \le \kappa_{\max}(n_\sigma,\fC)$

\item Computational cost $n \asymp (\sigma/\varepsilon)^2$ where $\sigma$ is unknown.

\end{itemize}

\item $\mu=\Ex(Y)=?$ for Bernoulli $Y$  \cite{JiaHic16a}
\begin{itemize}
\item Can find $n$ that guarantees that $\Prob(\abs{\mu - \hmu_n} \le \varepsilon_a) \ge 99\%$ or $\Prob(\abs{\mu - \hmu_n} \le \varepsilon_r \abs{\mu}) \ge 99\%$. 

\end{itemize}

\item $\mu=\int_a^b f(x) \, \dif x=?$  \cite{HicEtal14b}
\begin{itemize}
\item Can find $n$ that guarantees that the trapezoidal rule with $n$ trapezoids, $\hmu_n$, gives  $\abs{\mu - \hmu_n} \le \varepsilon$. 

\item Guaranteed for integrands in the \alert{cone} \medcone $\Var(f') \le \tau \norm[1]{f' - [f(b)-f(a)]/(b-a)}$

\item Computational cost $n \asymp \sqrt{\Var(f')/\varepsilon}$ where $\Var(f')$ is unknown.

\end{itemize}

\end{itemize}
\end{frame}

\section{Digital Net Cubature Error Bounds}
\begin{frame}[label=errbdslide]\frametitle{Digital Net Cubature Error via Walsh Expansions}
\tikzstyle{na} = [baseline=-0.3ex]
\tikzstyle{nb} = [baseline=0.5ex]
\tikzstyle{nc} = [baseline=-0.7ex]
\tikzstyle{nd} = [baseline=-1.7ex]
\vspace{-4ex}
\[
\Biggabs{\int_{\cube} f(\vx) \, \dif \vx -  \frac 1 {2^m} \sum_{i=0}^{2^m-1} f(\tikz[na] \node[baseline] (t1) {\!\!$\vz_i$\!\!};)} \le 
\sum_{\lambda=1}^\infty \bigabs{\tikz[nc] \node[baseline] (t2) {\!\!$\hf_{\lambda 2^m}$\!\!};} \le \frac{\homega(m) \wcomega(\ell) {\color{blue}\tS_{m-\ell,m}(f)} }{1 - \homega(\ell) \wcomega(\ell)} \beamerbutton{\hyperlink{SboundbytS}{proof}}
\]
\only<3>{\vspace{-4ex}}
\hspace{3.3cm}\alert<1>{digital net n}\tikz[nd] \node[coordinate] (n1) {};\alert<1>{odes}\hspace{1cm}Wa\tikz[nd] \node[coordinate] (n2) {};lsh coefficients in dual net
\vspace{-2ex}
\begin{tabular}{>{\centering}m{5cm}>{\centering}m{6.5cm}}
\only<1-2>{\alert<2>{Walsh func}\tikz[nb] \node[coordinate] (n3) {};\alert<2>{tions} \& \alert<2>{coeffi}\tikz[nb] \node[coordinate] (n4) {};\alert<2>{cients}}
\[
\begin{aligned}
\only<1-2>{f(\vx) &= \sum_{\kappa=0}^{\infty} \tikz[nc] \node[baseline] (t3) {$(-1)^{\ip{\vk(\kappa)}{\vx}}$};\!\!\!\! \tikz[nc] \node[baseline] (t4) {$\hf_{\kappa}$}; \\
\tf_{m,\kappa}&:= \frac{1}{2^m} \sum_{i=0}^{2^m-1} (-1)^{\ip{\tvk(\kappa)}{\vz_i}} f(\vz_i)\\
&=\sum_{\lambda=0}^{\infty} \hf_{\kappa+\lambda2^m} \ \text{aliasing}}
\only<3->{
{\color{red}\hS_{\ell,m}(f)} & := \sum_{\kappa=\lfloor 2^{\ell-1} \rfloor}^{2^{\ell}-1} \sum_{\lambda=1}^{\infty} \bigabs{ \hf_{\kappa+\lambda 2^{m}}}, \\
\wcS_m(f) & :=
\sum_{\kappa=2^{m}}^{\infty} \bigabs{\hf_{\kappa}} \\
{\color{blue}S_\ell(f)} &:=  \sum_{\kappa=\lfloor 2^{\ell-1} \rfloor}^{2^{\ell}-1} \bigabs{\hf_{\kappa}}\\
{\color{blue}\tS_{\ell,m}(f)} &:=\sum_{\kappa=\lfloor 2^{\ell-1} \rfloor}^{2^{\ell}-1} \bigabs{\tf_{m,\kappa}}}
\end{aligned}
\]
&
\only<1>{\includegraphics[width=5.7cm]{ProgramsImages/scrsob512pts.eps}}
\only<2>{
\includegraphics[width=1.8cm]{ProgramsImages/walshk1fun.eps}\ 
\includegraphics[width=1.8cm]{ProgramsImages/walshk2fun.eps}\ 
\includegraphics[width=1.8cm]{ProgramsImages/walshk3fun.eps}\\[2ex] 
\includegraphics[width=1.8cm]{ProgramsImages/walshk4fun.eps}\ 
\includegraphics[width=1.8cm]{ProgramsImages/walshk5fun.eps}\ 
\includegraphics[width=1.8cm]{ProgramsImages/walshk6fun.eps}
}
\only<3>{\includegraphics[width=6.3cm]{ProgramsImages/PlotFWTCoefUse256.eps}}
\end{tabular}
\only<3>{\vspace{-1ex}
\[
\text{\alert{Cone} \medcone conditions:} \quad {\color{red}\hS_{\ell,m}(f)} \le \homega(m-\ell) \wcS_m(f), \quad 
\wcS_m(f) \le \wcomega(\ell) {\color{blue}S_{m-\ell}(f)}.
\]}

\begin{tikzpicture}[overlay]
\path[red,thick,->] (n1) edge (t1);
\path[red,thick,->] (n2) edge (t2);
\only<1-2>{\path[red,thick,->] (n3) edge [out=-90, in=90] (t3);
\path[red,thick,->] (n4) edge (t4);}
\end{tikzpicture}
\end{frame}

\section{Attaining Error Tolerances}

\begin{frame}[label=algothm]\frametitle{Adaptively Attaining Absolute or Relative Error Tolerances}
Have $ \displaystyle 
\Biggabs{\underbrace{\int_{\cube} f(\vx) \, \dif \vx}_{\mu} -  \underbrace{\frac 1 {2^m} \sum_{i=0}^{2^m-1} f(\vz_i)}_{\hmu_m}} \le \underbrace{\frac{\homega(m) \wcomega(\ell) {\color{blue}\tS_{m-\ell,m}(f)} }{1 - \homega(\ell) \wcomega(\ell)}}_{\oerr(m)}$. \\
We want to find $m$ and $\tmu_m$ that guarantees
$
\abs{\mu-\tmu_m} \le \max(\varepsilon_a,\varepsilon_r \abs{\mu})
$.
\vspace{-2ex}
\alert{Algorithm \texttt{cubSobol\_g}.}  Given tolerances $\varepsilon_a$ and $\varepsilon_r$, fix $\ell$ and initalize $m>\ell$. 
\vspace{-1ex}
\begin{tabbing}
\alert{Step 1.}\  \=  Compute the \alert{data-based} error bound, $\oerr(m)$, and $\hmu_m$. \\ 
\alert{Step 2.}\> If $\oerr(m)$ is \alert{small enough} such that \\
\qquad $ \displaystyle
\oerr(m) \le \frac{1}{2}[\max(\varepsilon_a,\varepsilon_r \abs{\hmu_m - \oerr(m)} + \max(\varepsilon_a,\varepsilon_r \abs{\hmu_m + \oerr(m)}],
$\\
\> then return the \alert{shrinkage} estimator\\
\qquad $ \displaystyle
\tmu_m=\hmu_m + \frac{1}{2}[\max(\varepsilon_a,\varepsilon_r \abs{\hmu_m - \oerr(m)} \alert{-} \max(\varepsilon_a,\varepsilon_r \abs{\hmu_m + \oerr(m)}].
$ \\
\alert{Step 3.}\>  Otherwise, increase $m$ by one, and return to Step 1.
\end{tabbing}
\vspace{-3ex}
\alert{Theorem.}  For integrands satifying the \alert{cone} \medcone conditions \texttt{cubSobol\_g} \alert{succeeds} \beamerbutton{\hyperlink{tSboundbyS}{proof}}, and the computational \alert{cost} is $\Order([m + \$(f)]2^m )$, for some  
\vspace{-1ex}
\begin{multline*}
m \le \min \{m' : [1+ \homega(\ell) \wcomega(\ell)] (1 + \varepsilon_r) \homega(m') \wcomega(\ell) {\color{blue}S_{m'-\ell}(f)} \\ \le \max(\varepsilon_a,\varepsilon_r\abs{\mu})[1- \homega(\ell) \wcomega(\ell)] \} \beamerbutton{\hyperlink{oerrreq}{proof}}\beamerbutton{\hyperlink{tSboundbyS}{more proof}} \end{multline*}
\end{frame}

\begin{frame}[label=algothm]\frametitle{Observations about \texttt{cubSobol\_g}}
\alert{Theorem.}  For integrands satifying the \alert{cone} \medcone conditions \texttt{cubSobol\_g} \alert{succeeds}, and the computational \alert{cost} is $\Order([m + \$(f)]2^m )$, for some  
\vspace{-1ex}
\begin{multline*}
m \le \min \{m' : [1+ \homega(\ell) \wcomega(\ell)] (1 + \varepsilon_r) \homega(m') \wcomega(\ell) {\color{blue}S_{m'-\ell}(f)} \\ \le \max(\varepsilon_a,\varepsilon_r\abs{\mu})[1- \homega(\ell) \wcomega(\ell)] \} \end{multline*}
\vspace{-6ex}
\begin{itemize}

\item The \alert{error bound} and stopping criterion are  based on \alert{integrand values}, not norms of the integrand which are hard to compute.

\item The algorithm \alert{does not know the decay rate} of the Walsh coefficients but takes advantage of fast decay.

\item Because the algorithm is adaptive, upper bound on computational \alert{cost is based on characteristics of the integrand}, but these need not be known.

\item For relative error only ($\varepsilon_a=0$) the computational \alert{cost goes to $\infty$ as $\mu \to 0$}.

\item The \alert{extra cost} for not knowing $\mu$ or the true Walsh coefficients comes in the form of the constant multiple $[1+ \homega(\ell) \wcomega(\ell)] (1 + \varepsilon_r)$ on the left and $[1- \homega(\ell) \wcomega(\ell)]$ on the right.

\end{itemize}
\end{frame}

\section{Numerical Examples}
\begin{frame}\frametitle{Numerical Examples}
\begin{tabular}{>{\centering}m{5.7cm}>{\centering}m{5.7cm}}
Asian Geometric Mean Call Option \\
$d=1, 2, 4, \ldots, 64$
& Keister's \ycite{Kei96} Example \\[-4ex]
\[
\int_{\reals^d} \me^{-\norm{\vx}^2} \cos(\norm{\vx}) \, \dif \vx \vspace{-2ex}
\]
$d=1, \ldots, 19$
\tabularnewline
\includegraphics[width=5.7cm]{ProgramsImages/geomeancubSobolErrTime.eps} & 
\includegraphics[width=5.7cm]{ProgramsImages/KeistercubSobolErrTime.eps}\tabularnewline
$\approx 99\%$ success
&
$\approx 96\%$ success
\end{tabular}
\end{frame}


\section{Discussion}
\begin{frame} \frametitle{They Said It Couldn't Be Done!}

\begin{itemize}

\item \ocite{Lyn83} \alert{warned against adaptive algorithms} that use $\fC\abs{\hmu_{n} - \hmu_{n'}}$ as an error estimate.  We avoid error estimates of this type, but they are prevalent in existing adaptive algorithms. \\[3ex]

\item There are rather general sufficient conditions under which \alert{adaption provides no advantage} \citelist{\cite{BahSav56} \cite{TraWasWoz88}*{Chapter 4, Theorem 5.2.1} \cite{Nov96a}}.  To violate those conditions we consider \alert{nonconvex cones} \medcone of random variables or integrands.  \\[3ex]

\item We focus on \alert{cones} \medcone (instead of other shapes) because our problems are homogeneous and our error bounds are positively homogeneous.


\end{itemize}

\end{frame}


\begin{frame}\frametitle{Why Not Replications to Estimate Error?}
\vspace{-3ex}
\[
\Biggabs{\int_{\cube} f(\vx) \, \dif \vx 
- \hmu_n} \le \fC \sqrt{\frac{1}{R-1}\sum_{r=1}^R (Y_{r} - \hmu_n)^2},  \qquad \hmu_n:=\frac 1 R \sum_{r=1}^{R} Y_r
\]
\begin{description}

\item[IID Replications] $\displaystyle Y_{r}=\frac{R}{n}\sum_{i=0}^{n/R-1} f(\vz_i^{(r)})$, where $\{\vz_i^{(r)}\}$ are independent randomizations. 

\item[Internal Replications] $\displaystyle Y_{r}=\frac{R}{n}\sum_{i=(r-1)n/R}^{rn/R-1} f(\vz_i)$. 

\end{description}
Want $R$ small to take advantage of low discrepancy, but need $R$ large to ensure that sample variance of $Y_r$ represents error \cite{Den13a,HicEtal14a}.  

\end{frame}


\begin{frame} \frametitle{Guaranteed Automatic Integration Library (GAIL) \href{http://code.google.com/p/gail/}{\nolinkurl{code.google.com/p/gail/}}}

\hspace{-4ex}
\begin{tabular}{>{\centering}m{5cm}>{\centering}m{6.5cm}}
\begin{itemize}

\item Version 1.3 \cite{ChoEtal14a} includes \texttt{integral\_g.m}, \texttt{meanMC\_g.m}, \texttt{cubMC\_g.m}, \texttt{funappx\_g.m} 

\item Version 2.0 (exp.\ fall 2014) should include the following:
\begin{itemize} 
\item \texttt{cubSobol\_g.m}, \texttt{cubLattice\_g.m}
\item \texttt{meanBernoulli\_g.m}
\item \texttt{meanMC\_g.m}, \texttt{cubMC\_g.m} with absolue/relative error

\item \texttt{funappx\_g.m} with local adaption
\end{itemize}

\end{itemize}
&
\includegraphics[width=6.5cm]{ProgramsImages/GAILWebsiteButton.eps}
\end{tabular}

\end{frame}

\begin{frame} \frametitle{Future Work}

\begin{itemize}

\item Connections between our cone \medcone conditions and \alert{familiar spaces} of integrands, such as Korobov spaces\\[3ex]

\item Lower bound on the \alert{computational complexity}\\[3ex]

\item \alert{Multilevel} or \alert{multivariate decomposition method} algorithms\\[3ex]

\item Quasi-Monte Carlo for \alert{probabilities}\\[3ex]

\item \alert{Quantiles}

\end{itemize}

\end{frame}


\frame[allowframebreaks]{\frametitle{References}
\bibliography{FJH22,FJHown23}
}

\begin{frame}[label=SboundbytS]\frametitle{Proof of Cubature Error bound }
\vspace{-4ex}
\begin{align*}
{\color{blue}S_{\ell}(f)} &= \sum_{\kappa=\lfloor 2^{\ell-1} \rfloor}^{2^{\ell}-1} \bigabs{\hf_{\kappa}} = \sum_{\kappa=\lfloor 2^{\ell-1} \rfloor}^{2^{\ell}-1} \abs{\tf_{m,\kappa} - \sum_{\lambda=1}^{\infty} \hf_{\kappa+\lambda 2^{m}}} \\
& \le \underbrace{\sum_{\kappa=\lfloor 2^{\ell-1} \rfloor}^{2^{\ell}-1} \bigl \lvert \tf_{m,\kappa} \bigr\rvert}_{{\color{blue}\tS_{\ell,m}(f)}} + \underbrace{\sum_{\kappa=\lfloor 2^{\ell-1} \rfloor}^{2^{\ell}-1} \sum_{\lambda=1}^{\infty} \bigl \lvert \hf_{\kappa+\lambda 2^{m}}\bigr\rvert}_{\hS_{\ell,m}(f)} \\
&\le {\color{blue}\tS_{\ell,m}(f)} + \homega(m-\ell) \wcomega(m-\ell) {\color{blue}{S_{\ell}(f)}}  \\
{\color{blue}S_{\ell}(f)} & \le \frac{{\color{blue}\tS_{\ell,m}(f)}}{1 - \homega(m-\ell) \wcomega(m-\ell)} \qquad \text{provided that } \homega(m-\ell) \wcomega(m-\ell) < 1
\end{align*}
\begin{multline*}
\Biggabs{\int_{\cube} f(\vx) \, \dif \vx -  \frac 1 {2^m} \sum_{i=0}^{2^m-1} f(\vz_i)} \le 
\sum_{\lambda=1}^\infty \hf_{\lambda 2^m} = \hS_{0,m}(f) \\
\le \homega(m) \wcomega(\ell) {\color{blue}S_{\ell}(f)} \le \frac{\homega(m) \wcomega(\ell) {\color{blue}\tS_{\ell,m}(f)}}{1 - \homega(m-\ell) \wcomega(m-\ell)} 
\qquad \beamerreturnbutton{\hyperlink{errbdslide}{back}}\end{multline*}
\end{frame}

\begin{frame}[label=hybriderrproof]\frametitle{Proof that the Absolute/Relative Error Criterion is Met}
Define the average and half the difference of the error criterion at the lower and upper bounds for $\mu$: 
\[
\Delta_{m,\pm}:=\frac 1 2 \left[ \max\left(\varepsilon_a,\varepsilon_r\abs{\mu_m-\oerr(m)}\right) \pm \max\left(\varepsilon_a,\varepsilon_r\abs{\hmu_m+\oerr(m)}\right) \right]
\]
Then if $\tmu_m=\hmu_m+\Delta_{m,-}$ and $\abs{\mu-\hmu_m} \le \oerr(m) \le \Delta_{m,+}$, it follows that
\begin{align*}
\MoveEqLeft{0=\pm\left(\tmu_m-\hmu_m-\Delta_{n,-}\right)\leq \Delta_{n,+}-\oerr(m)}\\
\implies &\hmu_m+\Delta_{n,-} - \Delta_{n,+}+\oerr(m) \leq \tmu_m \leq \hmu_m+\Delta_{n,-} + \Delta_{n,+}-\oerr(m)\\
\implies & \hmu_m+\oerr(m)-\max\left(\varepsilon_a,\varepsilon_r \abs{\hmu_m+\oerr(m)}\right)\leq \tmu_m \leq \\
& \qquad \qquad \hmu_m-\oerr(m)+\max\left(\varepsilon_a,\varepsilon_r\abs{\hmu_m-\oerr(m)}\right) \\
\implies & \mu-\max\left(\varepsilon_a,\varepsilon_r \abs{\mu}\right)\leq \tmu_m \leq \mu+\max\left(\varepsilon_a,\varepsilon_r\abs{\mu}\right)\\
& \qquad \qquad \text{since } b\mapsto b\pm \max(\varepsilon_a,\varepsilon_r\abs{b})\text{ is non-decreasing} \\
\implies & \abs{\mu-\tmu_m} \le \max\left(\varepsilon_a,\varepsilon_r \abs{\mu}\right)
\end{align*}

 \hfill \hfill \beamerreturnbutton{\hyperlink{algothm}{back}}
\end{frame}

\begin{frame}[label=oerrreq]\frametitle{Bounding $\oerr(m)$ Required in Terms of $\mu$ and Tolerances}
Note that for $\abs{\mu-\hmu_m} \le \oerr(m)$ it follows that
\begin{align*}
\MoveEqLeft{\max(\varepsilon_a,\varepsilon_r\abs{\hmu_m+\sign(\hmu_m)\oerr(m)}) \geq \max(\varepsilon_a,\varepsilon_r\abs{\mu})}\\
\MoveEqLeft{\max(\varepsilon_a,\varepsilon_r\abs{\hmu_m-\sign(\hmu_m)\oerr(m)})} \\
& =\max(\varepsilon_a,\varepsilon_r\abs{\mu-\mu+\hmu_m-\sign(\hmu_m)\oerr(m)})\\
& \geq \max(\varepsilon_a,\varepsilon_r\abs{\mu})-\varepsilon_r\abs{-\mu+\hmu_m-\sign(\hmu_m)\oerr(m)} \\
& \geq \max(\varepsilon_a,\varepsilon_r\abs{\mu} )-2\varepsilon_r\oerr(m),
\intertext{which implies that}
\Delta_{+,m} 
&\ge \max(\varepsilon_a,\varepsilon_r\abs{\mu} )-\varepsilon_r\oerr(m)
\end{align*}
Therefore, if $\oerr(m)$ satisfies the inequality
\[
\oerr(m) \le \frac{\max(\varepsilon_a,\varepsilon_r\abs{\mu} )}{1 - \varepsilon_r}
\]
then the error condition of $\oerr(m) \le \Delta_{+,m} $ must be met.
\hfill \hfill \beamerreturnbutton{\hyperlink{algothm}{back}}
\end{frame}

\begin{frame}[label=tSboundbyS]\frametitle{Bounding $\oerr(m)$ in Terms of {\color{blue}$S_{\ell}(f)$}}
\vspace{-5ex}
\begin{align*}
{\color{blue}\tS_{\ell,m}(f)} &= \sum_{\kappa=\lfloor 2^{\ell-1} \rfloor}^{2^{\ell}-1} \bigabs{\tf_{m,\kappa}} = \sum_{\kappa=\lfloor 2^{\ell-1} \rfloor}^{2^{\ell}-1} \abs{\hf_{\kappa} + \sum_{\lambda=1}^{\infty} \hf_{\kappa+\lambda 2^{m}}} \\
& \le \underbrace{\sum_{\kappa=\lfloor 2^{\ell-1} \rfloor}^{2^{\ell}-1} \bigabs{ \hf_{\kappa}}}_{{\color{blue}S_{\ell}(f)}} + \underbrace{\sum_{\kappa=\lfloor 2^{\ell-1} \rfloor}^{2^{\ell}-1} \sum_{\lambda=1}^{\infty} \bigl \lvert \hf_{\kappa+\lambda 2^{m}}\bigr\rvert}_{\hS_{\ell,m}(f)} \\
&\le [1 + \homega(m-\ell) \wcomega(m-\ell)] {\color{blue}{S_{\ell}(f)}}
\intertext{which implies that}
\oerr(m) & = \frac{\homega(m) \wcomega(\ell) {\color{blue}\tS_{m-\ell,m}(f)} }{1 - \homega(\ell) \wcomega(\ell)}  \le  \frac{\homega(m) \wcomega(\ell) [1 + \homega(\ell) \wcomega(\ell)] {\color{blue}S_{m-\ell}(f)} }{1 - \homega(\ell) \wcomega(\ell)}  \text{ always}.
\end{align*}
Therefore, we know that $\oerr(m) \le \Delta_{+,m}$ must be satisfied when
\[
\frac{\homega(m) \wcomega(\ell) [1 + \homega(\ell) \wcomega(\ell)] {\color{blue}S_{m-\ell}(f)} }{1 - \homega(\ell) \wcomega(\ell)} \le \frac{\max(\varepsilon_a,\varepsilon_r\abs{\mu} )}{1 - \varepsilon_r}.
 \qquad \qquad \qquad \beamerreturnbutton{\hyperlink{algothm}{back}}
 \]
\end{frame}

\end{document}
